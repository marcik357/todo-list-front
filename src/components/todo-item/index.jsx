import { deleteTodoAction, changeTodoStatusAction } from '../../redux/actions/todoActions';
import { useDispatch, useSelector } from 'react-redux';

export default function TodoItem(props) {
    const dispatch = useDispatch()
    const todos = useSelector(state => state.todos.todos)

    function isDoneClickHandler(id) {
        const newTodos = [...todos.map(todo => {
            if (todo['_id'] !== id) return todo

            return todo.status === 'done'
                ? { ...todo, status: 'undone' }
                : { ...todo, status: 'done' }
        })]
        // const newTodos = [...todos.map(todo => todo['_id'] === id
        //     ? todo.status === 'done'
        //         ? { ...todo, status: 'undone' }
        //         : { ...todo, status: 'done' }
        //     : todo)]
        dispatch(changeTodoStatusAction(newTodos, id))
    }

    function removeTodoClickHandler(id) {
        dispatch(deleteTodoAction(id, todos))
    }

    return (
        <div id={props.id}>
            <span style={{ textDecoration: props.status === 'done' ? 'line-through' : 'none' }}>{props.text}</span>
            <button onClick={() => { isDoneClickHandler(props.id) }}>mark as {props.status === 'done' ? 'undone' : 'done'}</button>
            <button onClick={() => { removeTodoClickHandler(props.id) }}>remove from list</button>
        </div>
    )
}