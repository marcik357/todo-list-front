import { useEffect, useState } from 'react';
import TodoItem from '../todo-item';
import { useDispatch, useSelector } from 'react-redux';
import { postTodoAction, getAllTodosAction } from '../../redux/actions/todoActions';

export default function TodoList() {
    const dispatch = useDispatch()
    const [isNewTodo, setNewTodo] = useState(false)
    const todos = useSelector(state => state.todos.todos)

    const [newTodoText, setNewTodoText] = useState('')

    useEffect(() => {
        dispatch(getAllTodosAction())
    }, [dispatch])

    function onSubmitHandler() {
        dispatch(postTodoAction(newTodoText))
        setNewTodo(false)
        setNewTodoText('')
    }

    function onCancelHandler() {
        setNewTodo(false)
    }

    return (
        <div>
            <h2>My awesome todo list</h2>
            {todos.length > 0
                ? todos.map((todo) => (<TodoItem key={todo['_id']} id={todo['_id']} text={todo.text} status={todo.status} />))
                : <p>Loading...</p>}
            {isNewTodo ? (
                <>
                    <input onChange={(e) => setNewTodoText(e.currentTarget.value)} value={newTodoText} placeholder='insert your todo'></input>
                    <button onClick={() => onSubmitHandler()}>add</button>
                    <button onClick={() => onCancelHandler()}>cancel</button>
                </>
            ) : (
                <button onClick={() => setNewTodo(true)}>new todo</button>
            )}
        </div>
        
    )
}