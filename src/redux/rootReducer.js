import { combineReducers } from 'redux';
import { todoReducer as todos } from "./reducers/todoReducer";

export const rootReducer = combineReducers({
    todos
});

