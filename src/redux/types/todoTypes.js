export const todoTypes = {
    todoAddAll: 'todos/todoAddedAll',
    todoAdd: 'todos/todoAdded',
    todoDone: 'todos/todoDone',
    todoRemove: 'todos/todoRemove',
}