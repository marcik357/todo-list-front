import { fetchData } from "../../utils";
import { todoTypes } from "../types/todoTypes";

const url = 'https://todo-list-back-six.vercel.app/todos/'

export function getAllTodosAction() {
    return async (dispatch) => {
        try {
            const data = await fetchData(url)
            dispatch(addAllTodosAction(data))
        } catch (error) {
            console.log(error)
        }
    }
}

export function addAllTodosAction(data) {
    return {
        type: todoTypes.todoAddAll,
        payload: data
    }
}

export function postTodoAction(text) {
    return async (dispatch) => {
        try {
            const todo = {
                text: text,
                status: 'undone'
            }
            const response = await fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(todo)
            })
            const data = await response.json()
            dispatch(addTodoAction(data))
        } catch (error) {
            console.log(error)
        }
    }
}

export function addTodoAction(todo) {
    return {
        type: todoTypes.todoAdd,
        payload: todo
    }
}

export function changeTodoStatusAction(todos, id) {
    return async (dispatch) => {
        try {
            const editedTodo = todos.find(todo => todo['_id'] === id)
            const response = await fetch(url + id, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(editedTodo)
            })

            if (!response.ok) {
                throw new Error('Error in isDoneTodo');
            }

            dispatch(isDoneTodoAction(todos))
        } catch (error) {
            console.log(error)
        }
    }
}

export function deleteTodoAction(id, todos) {
    return async (dispatch) => {
        try {
            const newTodos = [...todos.filter(todo => todo['_id'] !== id)]
            const response = await fetch(url + id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            if (!response.ok) {
                throw new Error('Error in delete');
            }
            dispatch(removeTodoAction(newTodos))
        } catch (error) {
            console.log(error)
        }
    }
}

export function isDoneTodoAction(todos) {
    return {
        type: todoTypes.todoDone,
        payload: todos
    }
}

export function removeTodoAction(todos) {
    return {
        type: todoTypes.todoRemove,
        payload: todos
    }
}