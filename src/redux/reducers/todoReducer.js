import { getDataFromLS } from "../../utils";
import { todoTypes } from "../types/todoTypes";

const initialState = {
    todos: getDataFromLS('todos')
}

export const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case todoTypes.todoAdd:
            return { ...state, todos: [...state.todos, action.payload] }
        case todoTypes.todoAddAll:
            return { ...state, todos: action.payload }
        case todoTypes.todoDone:
            return { ...state, todos: action.payload }
        case todoTypes.todoRemove:
            return { ...state, todos: action.payload }

        default:
            return state;
    }
}