# Frontend для простого Todo

- Зберігання тудушек в Redux і MongoDB
- Додавання нових, видаляння, відмітка про виконання

- Деплой на [Vercel](https://todo-list-front-rho.vercel.app/)

- Деплой backend на [Vercel](https://todo-list-back-six.vercel.app/)
